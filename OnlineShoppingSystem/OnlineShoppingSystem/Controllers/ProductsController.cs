﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using OnlineShoppingSystem.Models;

namespace OnlineShoppingSystem.Controllers
{
    public class ProductsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
       // [Authorize(Roles = "Customer")]
        // GET: Products
        public ActionResult Index()
        {
            var userName = db.Customers.Where(u => u.CustomerEmail == User.Identity.Name).Select(k => k.CustomerName).FirstOrDefault();
            ViewBag.CustomerName = userName;
            var customerId = db.Customers.Where(j => j.CustomerEmail == User.Identity.Name).Select(o => o.CustomerId).FirstOrDefault();
            var itemCount= db.CartItems.Where(k => k.CustomerId == customerId && k.OrderId == 0).Select(k => k.CartQuantity).ToList();
            if (itemCount.Count==0)
            {
                ViewBag.CartItemsNo = 0;
            }
            else
            {
                ViewBag.CartItemsNo = itemCount.Count();
            }
          

            var products = db.Products.Include(p => p.Department);
            return View(products.ToList());
        }
        [Authorize(Roles ="Customer")]
        public ActionResult CartItemsList()
        {
            var customerId = db.Customers.Where(j => j.CustomerEmail == User.Identity.Name).Select(o => o.CustomerId).FirstOrDefault();
            var itemCount= db.CartItems.Where(k => k.CustomerId == customerId && k.OrderId == 0).Select(k => k.CartQuantity).ToList();
            if (itemCount.Count==0)
            {
                ViewBag.CartItemsNo = 0;
            }
            else
            {
                ViewBag.CartItemsNo = itemCount.Count();
            }
           

            var cartProducts = db.CartItems.Where(k => k.CustomerId == customerId && k.OrderId == 0).Include(p => p.Product);
            return View(cartProducts.ToList());
        }

        [HttpPost]
        [Authorize(Roles ="Customer")]
        public JsonResult SaveCartItems(CartItem cartItem)
        {
            var customerId = db.Customers.Where(j => j.CustomerEmail == User.Identity.Name).Select(o => o.CustomerId).FirstOrDefault();
             var existCartItem= db.CartItems.Where(l => l.CustomerId == customerId && l.ProductId == cartItem.ProductId && l.OrderId == 0).FirstOrDefault();
            
            if (existCartItem == null)
            {
                cartItem.CustomerId = customerId;
                cartItem.OrderId = 0;
                db.CartItems.Add(cartItem);
                db.SaveChanges();
            }
            else
            {
                existCartItem.CartQuantity += cartItem.CartQuantity;
                db.Entry(existCartItem).State = EntityState.Modified;
                db.SaveChanges();
            }

            if (cartItem.CartId > 0||existCartItem.CartQuantity>0)
            {
                ViewBag.CartItemsNo = db.CartItems.Where(k => k.CustomerId == customerId && k.OrderId == 0).Select(k => k.CartQuantity).Count();
                return new JsonResult { Data = new { status = true,url=Url.Action("Index") } };
            }

            return new JsonResult { Data = new { status = false } };
        }

        // GET: Products/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }
        
        [Authorize(Roles="admin")]
        // GET: Products/Create
        public ActionResult Create()
        {
            ViewBag.DepartmentId = new SelectList(db.Departments, "DepartmentId", "DepartmentName");
            return View();
        }

        // POST: Products/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Exclude  = "ProductPic")] Product product)
        {
            if (ModelState.IsValid)
            {
                product.ProductPic = getFileById("ProductPic");
                db.Products.Add(product);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.DepartmentId = new SelectList(db.Departments, "DepartmentId", "DepartmentName", product.DepartmentId);
            return View(product);
        }

        // GET: Products/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            ViewBag.DepartmentId = new SelectList(db.Departments, "DepartmentId", "DepartmentName", product.DepartmentId);
            return View(product);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ProductId,ProductName,ProductPrice,DepartmentId")] Product product)
        {
            if (ModelState.IsValid)
            {
                db.Entry(product).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DepartmentId = new SelectList(db.Departments, "DepartmentId", "DepartmentName", product.DepartmentId);
            return View(product);
        }

        private byte[] getFileById(string id)
        {
            byte[] fileData = null;
            HttpPostedFileBase poImgFile = Request.Files[id];

            using (var binary = new BinaryReader(poImgFile.InputStream))
            {
                fileData = binary.ReadBytes(poImgFile.ContentLength);
            }

            return fileData;
        }

        // GET: Products/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Product product = db.Products.Find(id);
            db.Products.Remove(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
