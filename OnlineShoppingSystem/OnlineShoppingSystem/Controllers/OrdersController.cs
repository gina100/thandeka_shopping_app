﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using OnlineShoppingSystem.Models;

namespace OnlineShoppingSystem.Controllers
{
    public class OrdersController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Orders
        public ActionResult Index()
        {
            return View(db.Orders.ToList());
        }

        [Authorize(Roles = "Customer")]
        public ActionResult CreateOrder()
        {
            var customerId = db.Customers.Where(j => j.CustomerEmail == User.Identity.Name).Select(o => o.CustomerId).FirstOrDefault();
            var itemCount = db.CartItems.Where(k => k.CustomerId == customerId && k.OrderId == 0).Select(k => k.CartQuantity).ToList();
            if (itemCount.Count == 0)
            {
                ViewBag.CartItemsNo = 0;
            }
            else
            {
                ViewBag.CartItemsNo = itemCount.Count();
            }
            
            var cartProducts = db.CartItems.Where(k => k.CustomerId == customerId && k.OrderId == 0).Include(p => p.Product);
            OrderCartList orderCartList = new OrderCartList();
            orderCartList.CartList = cartProducts.ToList();

            return View(orderCartList);
        }

        [HttpPost]
        public JsonResult CreateOrder(string DestinationAddress)
        {
            var customerId = db.Customers.Where(j => j.CustomerEmail == User.Identity.Name).Select(o => o.CustomerId).FirstOrDefault();
            var cartList = db.CartItems.Where(l => l.CustomerId == customerId && l.OrderId == 0).ToList();
            var totalPrice = 0;
            foreach(var item in cartList)
            {
                totalPrice += item.Product.ProductPrice * item.CartQuantity;
            }

            Order order = new Order();
            order.CustomerId = customerId;
            order.DestinationAddress = DestinationAddress;
            order.TotalAmount = totalPrice;
            order.RefNo ='#'+ Guid.NewGuid().ToString().Substring(0, 10);
            order.OrderStatus = "Order Processed";
            order.ExpectedArrivalDate = DateTime.Now.AddDays(10).ToString("MM/dd/yyyy");
            db.Orders.Add(order);
            db.SaveChanges();

            foreach(var item in cartList)
            {
                item.OrderId = order.OrderId;
                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();
            }

            return new JsonResult { Data = new { status = true, url = Url.Action("Create", "Payment",new { orderId=order.OrderId,customerId=customerId,totAmount=order.TotalAmount}) } };
        }

        // GET: Orders/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // GET: Orders/Create
        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Track(int id)
        {
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // POST: Orders/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "OrderId,AmountPaid,TotalAmount,DestinationAddress")] Order order)
        {
            if (ModelState.IsValid)
            {
                db.Orders.Add(order);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(order);
        }

        // GET: Orders/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // POST: Orders/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Order orderItem)
        {
            Order order = db.Orders.Find(orderItem.OrderId);
            if (ModelState.IsValid)
            {
                order.OrderStatus = orderItem.OrderStatus;
                db.Entry(order).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(order);
        }

        // GET: Orders/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // POST: Orders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Order order = db.Orders.Find(id);
            db.Orders.Remove(order);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
