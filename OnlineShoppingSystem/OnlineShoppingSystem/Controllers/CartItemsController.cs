﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using OnlineShoppingSystem.Models;

namespace OnlineShoppingSystem.Controllers
{
    public class CartItemsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: CartItems
        public ActionResult Index()
        {
            var cartItems = db.CartItems.Include(c => c.Customer).Include(c => c.Product);
            return View(cartItems.ToList());
        }

        // GET: CartItems/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CartItem cartItem = db.CartItems.Find(id);
            if (cartItem == null)
            {
                return HttpNotFound();
            }
            return View(cartItem);
        }

        // GET: CartItems/Create
        public ActionResult Create()
        {
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "CustomerName");
            ViewBag.OrderId = new SelectList(db.Orders, "OrderId", "AmountPaid");
            ViewBag.ProductId = new SelectList(db.Products, "ProductId", "ProductName");
            return View();
        }

        // POST: CartItems/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CartId,CustomerId,ProductId,CartQuantity,OrderId")] CartItem cartItem)
        {
            if (ModelState.IsValid)
            {
                db.CartItems.Add(cartItem);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "CustomerName", cartItem.CustomerId);
            ViewBag.OrderId = new SelectList(db.Orders, "OrderId", "AmountPaid", cartItem.OrderId);
            ViewBag.ProductId = new SelectList(db.Products, "ProductId", "ProductName", cartItem.ProductId);
            return View(cartItem);
        }

        // GET: CartItems/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CartItem cartItem = db.CartItems.Find(id);
            if (cartItem == null)
            {
                return HttpNotFound();
            }
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "CustomerName", cartItem.CustomerId);
            ViewBag.OrderId = new SelectList(db.Orders, "OrderId", "AmountPaid", cartItem.OrderId);
            ViewBag.ProductId = new SelectList(db.Products, "ProductId", "ProductName", cartItem.ProductId);
            return View(cartItem);
        }

        [HttpPost]
        public JsonResult UpdateCartItem(int? cartId,int quantity)
        {
            if (cartId == null)
            {
                return new JsonResult { Data = new { status = false } };
            }
            CartItem cartItem = db.CartItems.Find(cartId);
            cartItem.CartQuantity = quantity;

            db.Entry(cartItem).State = EntityState.Modified;
            db.SaveChanges();

            return new JsonResult { Data = new { status = true, url = Url.Action("CartItemsList", "Products") } };
        }

        // POST: CartItems/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CartId,CustomerId,ProductId,CartQuantity,OrderId")] CartItem cartItem)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cartItem).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "CustomerName", cartItem.CustomerId);
            ViewBag.OrderId = new SelectList(db.Orders, "OrderId", "AmountPaid", cartItem.OrderId);
            ViewBag.ProductId = new SelectList(db.Products, "ProductId", "ProductName", cartItem.ProductId);
            return View(cartItem);
        }

        // GET: CartItems/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CartItem cartItem = db.CartItems.Find(id);
            if (cartItem == null)
            {
                return HttpNotFound();
            }
            return View(cartItem);
        }
        
        [HttpPost]
        public JsonResult DeleteCartItem(int? cartId)
        {
            if (cartId == null)
            {
                return  new JsonResult { Data = new { status = false } };
            }
            CartItem cartItem = db.CartItems.Find(cartId);
            db.CartItems.Remove(cartItem);
            db.SaveChanges();
            return new JsonResult { Data = new { status = true, url = Url.Action("CartItemsList","Products") } };
        }
    
        // POST: CartItems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CartItem cartItem = db.CartItems.Find(id);
            db.CartItems.Remove(cartItem);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
