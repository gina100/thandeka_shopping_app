﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using OnlineShoppingSystem.Models;

namespace OnlineShoppingSystem.Controllers
{
    public class SuppliedItemsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: SuppliedItems
        public ActionResult Index()
        {
            var suppliedItems = db.SuppliedItems.Include(s => s.Product).Include(s => s.Supplier);
            return View(suppliedItems.ToList());
        }

        // GET: SuppliedItems/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SuppliedItem suppliedItem = db.SuppliedItems.Find(id);
            if (suppliedItem == null)
            {
                return HttpNotFound();
            }
            return View(suppliedItem);
        }

        // GET: SuppliedItems/Create
        public ActionResult Create()
        {
            ViewBag.ProductId = new SelectList(db.Products, "ProductId", "ProductName");
            ViewBag.SupplierId = new SelectList(db.Suppliers, "SupplierId", "SupplierName");
            return View();
        }

        // POST: SuppliedItems/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SupplierCode,SuppliedItemQuantity,SupplierId,ProductId")] SuppliedItem suppliedItem)
        {
            if (ModelState.IsValid)
            {
                db.SuppliedItems.Add(suppliedItem);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ProductId = new SelectList(db.Products, "ProductId", "ProductName", suppliedItem.ProductId);
            ViewBag.SupplierId = new SelectList(db.Suppliers, "SupplierId", "SupplierName", suppliedItem.SupplierId);
            return View(suppliedItem);
        }

        // GET: SuppliedItems/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SuppliedItem suppliedItem = db.SuppliedItems.Find(id);
            if (suppliedItem == null)
            {
                return HttpNotFound();
            }
            ViewBag.ProductId = new SelectList(db.Products, "ProductId", "ProductName", suppliedItem.ProductId);
            ViewBag.SupplierId = new SelectList(db.Suppliers, "SupplierId", "SupplierName", suppliedItem.SupplierId);
            return View(suppliedItem);
        }

        // POST: SuppliedItems/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SupplierCode,SuppliedItemQuantity,SupplierId,ProductId")] SuppliedItem suppliedItem)
        {
            if (ModelState.IsValid)
            {
                db.Entry(suppliedItem).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ProductId = new SelectList(db.Products, "ProductId", "ProductName", suppliedItem.ProductId);
            ViewBag.SupplierId = new SelectList(db.Suppliers, "SupplierId", "SupplierName", suppliedItem.SupplierId);
            return View(suppliedItem);
        }

        // GET: SuppliedItems/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SuppliedItem suppliedItem = db.SuppliedItems.Find(id);
            if (suppliedItem == null)
            {
                return HttpNotFound();
            }
            return View(suppliedItem);
        }

        // POST: SuppliedItems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SuppliedItem suppliedItem = db.SuppliedItems.Find(id);
            db.SuppliedItems.Remove(suppliedItem);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
