﻿using OnlineShoppingSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineShoppingSystem.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public ActionResult Index()
        {
            var userName = db.Customers.Where(u => u.CustomerEmail == User.Identity.Name).Select(k => k.CustomerName).FirstOrDefault();
            var customerId = db.Customers.Where(j => j.CustomerEmail == User.Identity.Name).Select(o => o.CustomerId).FirstOrDefault();
            ViewBag.CustomerName = userName;
            var itemCount = db.CartItems.Where(k => k.CustomerId == customerId && k.OrderId == 0).Select(k => k.CartQuantity).ToList();
            if (itemCount.Count == 0)
            {
                ViewBag.CartItemsNo = 0;
            }
            else
            {
                ViewBag.CartItemsNo = itemCount.Count();
            }

             var products = db.Products;
            return View(products.ToList());
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}