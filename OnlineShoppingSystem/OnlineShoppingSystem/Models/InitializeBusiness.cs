﻿using System.Data.Entity;

namespace OnlineShoppingSystem.Models
{
    public static class InitializeBusiness
    {
        public static void Initialize()
        {
            Database.SetInitializer<ApplicationDbContext>(new DbInitialize<ApplicationDbContext>());
            var ctx = new ApplicationDbContext();
            ctx.Database.Initialize(true);
        }
    }
}