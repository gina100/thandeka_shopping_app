﻿
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace OnlineShoppingSystem.Models
{
    public class DbInitialize<T> : DropCreateDatabaseIfModelChanges<IdentityDbContext>
    {
        protected override void Seed(IdentityDbContext context)
        {
            var userManager = new UserManager<ApplicationUser>(new
                                            UserStore<ApplicationUser>(context));

            var roleManager = new RoleManager<ApplicationRole>(new
                                      RoleStore<ApplicationRole>(context));

            const string name = "thandekaweb@gmail.com";
            const string rol = "admin";
            const string password = "#2021Password";

            if (!roleManager.RoleExists(rol))
            {
                var roleresult = roleManager.Create(new ApplicationRole(rol));
            }

            if (!roleManager.RoleExists("Customer"))
            {
                var roleresult = roleManager.Create(new ApplicationRole("Customer"));
            }

            var userAdmin = new ApplicationUser();
            var userCustomer = new ApplicationUser();
            var db = new ApplicationDbContext();
            var customer = new Customer();
            
            
            userAdmin.UserName = name;
            // userAdmin.userStatus = "No";
            userCustomer.UserName = "customer@gmail.com";

            

            var adminresult = userManager.Create(userAdmin, password);
            var customerResult = userManager.Create(userCustomer, password);


            if (adminresult.Succeeded)
            {
                var result = userManager.AddToRole(userAdmin.Id, rol);
            }
            if (customerResult.Succeeded)
            {
                var result = userManager.AddToRole(userCustomer.Id, "Customer");
                customer.CustomerEmail = userCustomer.UserName;
                customer.CustomerName = "Thandeka";
                customer.CustomerPlace = "Durban";
                customer.CustomerGender = "Female";
                customer.CustomerOccupation = "Student";
                customer.CustomerMobileNumber = "0653066402";
                customer.CustomerDateOfBirth = new DateTime(1999, 04, 11);
                db.Customers.Add(customer);
                db.SaveChanges();
            }
            
            base.Seed(context);
        }
    }
}