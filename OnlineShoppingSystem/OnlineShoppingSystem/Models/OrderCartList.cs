﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OnlineShoppingSystem.Models
{
    public class OrderCartList
    {
        public int OrderId { get; set; }
        [Display(Name = "Amount")]
        public string AmountPaid { get; set; }
        [Display(Name = "Total")]
        public string TotalAmount { get; set; }
        [Display(Name = "Destination")]
        public string DestinationAddress { get; set; }
        [ForeignKey("Customer")]
        public int CustomerId { get; set; }
        public virtual Customer Customer { get; set; }
        public List<CartItem> CartList { get; set; }
    }
}