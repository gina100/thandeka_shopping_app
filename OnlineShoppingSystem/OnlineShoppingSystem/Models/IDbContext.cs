﻿using System.Data.Entity;

namespace OnlineShoppingSystem.Models
{
    public interface IDbContext
    {
        IDbSet<TEntity> Set<TEntity>() where TEntity : class;
        int SaveChanges();
        void Dispose();
    }
}