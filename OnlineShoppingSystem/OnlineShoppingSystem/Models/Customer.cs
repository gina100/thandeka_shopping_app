﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
//using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OnlineShoppingSystem.Models
{
    public class Customer
    {
        [Key]
        
        public int CustomerId { get; set; }
        [Display(Name ="Name")]
        public string CustomerName { get; set; }
        [Display(Name = "Address")]
        public string CustomerPlace { get; set; }
        [Display (Name = "Gender")]
        public string CustomerGender { get; set; }
        [Display(Name ="Occupation")]
        public string CustomerOccupation { get; set; }
        [Display(Name = "Mobile Number")]
        public string CustomerMobileNumber { get; set; }
        [Display(Name = "Email")]
        public string CustomerEmail { get; set; }
        [Display(Name = "Date of Birth")]
        public DateTime CustomerDateOfBirth { get; set; }
        public ICollection<CartItem> CartItems { get; set; }
        public ICollection<Order> Orders { get; set; }
        public ICollection<Customer> Customers{ get; set; }
    }

}