﻿
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OnlineShoppingSystem.Models
{
    public class Order
    {
        [Key]
        public int OrderId { get; set; }
        [Display(Name = "Ref No.")]
        public string RefNo { get; set; }
        [Display(Name = "Order Status")]
        public string OrderStatus { get; set; }
       
        [Display(Name = "Total")]
        public int TotalAmount { get; set; }
        [Display(Name = "Destination")]
        public string DestinationAddress { get; set; }
        [Display(Name = "Expected Arrival Date")]
        public string ExpectedArrivalDate { get; set; }
        [ForeignKey("Customer")]
        public int CustomerId { get; set; }
        public virtual Customer Customer { get; set; }
    }
}