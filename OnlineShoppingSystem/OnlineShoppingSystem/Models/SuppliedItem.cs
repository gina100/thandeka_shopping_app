﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace OnlineShoppingSystem.Models
{
    public class SuppliedItem
    {
        [Key]
        public int SupplierCode { get; set; }
        
        [Display(Name = "Quantity")]
        public string SuppliedItemQuantity { get; set; }
        [ForeignKey("Supplier")]
        public int SupplierId { get; set; }
        public virtual Supplier Supplier { get; set; }
        [ForeignKey("Product")]
        public int ProductId { get; set; }
        public virtual Product Product { get; set; }
       

    }
}