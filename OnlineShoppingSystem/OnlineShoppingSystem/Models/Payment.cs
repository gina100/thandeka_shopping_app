﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OnlineShoppingSystem.Models
{
    public class Payment
    {
        [Key]
        public int PaymentId { get; set; }
        public int CustomerId { get; set; }
      
        [DisplayName("Reference No.")]
        public int OrderId { get; set; }
        public DateTime DatetimeCreated { get; set; }
       
        [DisplayName("Amount")]
        public double AmountPayed { get; set; }
    }
}