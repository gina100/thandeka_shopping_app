﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace OnlineShoppingSystem.Models
{
    public class Supplier
    {
        [Key]
        public int SupplierId { get; set; }
        [Display(Name = "Name")]
        public string SupplierName { get; set; }
        [Display(Name = "Address")]
        public string PhysicalAddress { get; set; }
        [ForeignKey("SupplierCode")]
        public ICollection<SuppliedItem> SuppliedItem { get; set; }
       
    }
}