﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace OnlineShoppingSystem.Models
{
    public class Product
    {
        [Key]
        public int ProductId { get; set; }
        [Display(Name ="Product Name")]
        public string ProductName { get; set; }
        [Display(Name = "Price")]
        public int ProductPrice { get; set; }
        public string ProductSize{ get; set; }
        public string ProductColor { get; set; }
        [Display(Name ="Picture")]
        public byte[] ProductPic { get; set; }
        public virtual Department Department { get; set; }
        [ForeignKey("Department")]
        public int DepartmentId { get; set; }
        [ForeignKey("CartId")]
        public ICollection<CartItem> CartItems { get; set; }
        [ForeignKey("SupplierCode")]
        public ICollection<SuppliedItem> SuppliedItems { get; set; }
    }
}