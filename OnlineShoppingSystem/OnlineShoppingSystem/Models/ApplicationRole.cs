﻿
using Microsoft.AspNet.Identity.EntityFramework;

namespace OnlineShoppingSystem.Models
{
    public class ApplicationRole : IdentityRole
    {
        public ApplicationRole()
        {

        }

        public ApplicationRole(string roleName)
            : base(roleName)
        {
        }
    }
}