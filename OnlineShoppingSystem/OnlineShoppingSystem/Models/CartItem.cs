﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace OnlineShoppingSystem.Models
{
    public class CartItem
    {
        [Key]
        public int CartId { get; set;}
        [ForeignKey("Customer")]
        public int CustomerId { get; set; }
        [ForeignKey("Product")]
        public int ProductId { get; set; }
        [Display(Name= "Quantity")]
        public int CartQuantity { get; set; }
        [Display(Name ="Order No.")]
        //[ForeignKey("Order")]
        public int OrderId { get; set; }
       // public virtual Order Order { get; set; }
        public virtual Customer Customer{ get; set; }
        public virtual Product Product { get; set; }

    }
}